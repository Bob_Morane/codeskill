package paka;

import java.util.Arrays;

/**
 * Created by nerd on 08.04.17.
 */
public class ObrocImieNazwisko {

    public static void main(String[] args) {
        String tablica[] =
                {"Imie","|","Wzrost","\n",
                 "Zofia","|","165","\n",
                 "Andrzej","|","187","\n",
                 "Heniek","|","181","\n"};

        for(String x: tablica)
            System.out.print(x);

        String tablica2[] = new String[tablica.length];

        int j=0,i=0;
        for (i=0;i<tablica.length;i+=4){
            tablica2[j++]=tablica[i];
            tablica2[j++]="|";
        }
        tablica2[j-1]="\n";

        for (i=2;i<tablica.length;i+=4){
            tablica2[j++]=tablica[i];
            tablica2[j++]="|";
        }
        tablica2[j-1]="\n";

        System.out.println();

        for(String x: tablica2)
            System.out.print(x);
    }


}
