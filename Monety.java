package paka;

import java.util.Scanner;

/**
 * Created by nerd on 06.04.17.
 */
public class Monety {

    static void reszta(int kwota){
        int ilePiatek = kwota/5;
        int ileDwojek = kwota%5/2;
        int ileZlotowek = kwota%5%2;
        int ileMonet = ilePiatek + ileDwojek + ileZlotowek;

        System.out.print(kwota + " => ");
        if(kwota<=0){
            System.out.println(0);
            return;
        }
        while (ilePiatek>0) {
            System.out.print("5");
            ilePiatek--;
            if(--ileMonet>0) {
                System.out.print(",");
            }
            else {
                System.out.println();
                return;
            }
        }
        while (ileDwojek>0) {
            System.out.print("2");
            ileDwojek--;
            if(--ileMonet>0) {
                System.out.print(",");
            }
            else {
                System.out.println();
                return;
            }
        }
        while (ileZlotowek>0) {
            System.out.print("1");
            ileZlotowek--;
            if(--ileMonet>0) {
                System.out.print(",");
            }
            else {
                System.out.println();
                return;
            }
        }
    }

    public static void main(String[] args) {
        reszta(0);
        reszta(3);
        reszta(8);
        reszta(12);
    }
}
